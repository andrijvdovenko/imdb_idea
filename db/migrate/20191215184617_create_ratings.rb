class CreateRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :ratings do |t|
      t.belongs_to :user, index: true
      t.belongs_to :movie, index: true
      t.integer :rate, default: 0, presence: true

      t.timestamps
    end
  end
end
