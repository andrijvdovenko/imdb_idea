%w[user admin].each do |role|
  Role.find_or_create_by(name: role)
end

User.create(email: 'admin@imdb.com', password: '123qwerty', role: Role.find_or_create_by(name: 'admin'))

%w[Adventure Action Thriller Horrror Comedy Musical Romance Drama Fantasy].each do |category|
  Category.find_or_create_by(title: category)
end
