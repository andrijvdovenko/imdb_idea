module MoviesHelper
  def rate_options
    [].tap do |a|
      @ratings.each do |k|
        a << [k, k]
      end
    end
  end
end
