class MoviesController < ApplicationController
  before_action :authenticate_user!, except: %i[index update]
  before_action :set_movie, only: %i[show edit update destroy]
  before_action :set_rate, only: %i[show edit]
  before_action :set_ratings, :set_categoties

  def index
    @movies = Movie.all

    update_rate(params['movie_id'], params['rate']) if params['movie_id'] && params['rate']

    filter = params[:category]
    category = Category.find_by(title: filter)

    @movies = @movies.where(category_id: category.id) if filter.present? && category
    @movies = @movies.page(params[:page])

    respond_to do |format|
      format.html
      format.json { render html: @movies }
    end
  end

  def new
    @movie = Movie.new
  end

  def edit; end

  def show; end

  def create
    @movie = Movie.new(movie_params)
    @movie.save

    redirect_to movie_path @movie
  end

  def update
    @movie.update(movie_params)

    update_rate('', rating_params['ratings']) if rating_params['ratings'].present? && rating_params['ratings'] != 0
    #   rating = Rating.find_or_initialize_by(movie: @movie, user: current_user)
    #   rating.update(rate: rating_params['ratings'])
    # end

    redirect_to movie_path @movie
  end

  def destroy
    @movie.destroy

    redirect_to movies_path
  end

  private

  def set_movie
    @movie = Movie.find(params[:id])
  end

  def set_rate
    @rate = Rating.find_by(movie_id: @movie.id, user: current_user)&.rate
  end

  def movie_params
    params.require(:movie).permit(:title, :text, :category_id)
  end

  def rating_params
    params.require(:movie).permit(:ratings)
  end

  def set_ratings
    @ratings = Rating.rates.keys
  end

  def set_categoties
    @categories = Category.all
  end

  def filtering_params(params)
    params.slice(:category)
  end

  def update_rate(id, rate)
    movie = @movie || Movie.find(id)
    rating = Rating.find_or_initialize_by(movie: movie, user: current_user)
    rating.update(rate: rate)
  end
end
