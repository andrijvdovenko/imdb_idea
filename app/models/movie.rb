class Movie < ApplicationRecord
  has_many :ratings
  has_many :users, through: :ratings
  belongs_to :category

  paginates_per 10

  def current_rate
    r = ratings.map(&:rate)

    return 0 if r.empty?

    r.sum(&:to_i) / r.count
  end

  def user_rate(user)
    if user && ratings.find_by(user: user).present?
      ratings.find_by(user: user).rate
    else
      current_rate
    end
  end
end
