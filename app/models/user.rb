class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :ratings
  has_many :movies, through: :ratings
  belongs_to :role

  before_create :set_default_role

  def to_s
    email
  end

  def admin?
    role.to_s == 'admin'
  end

  def user?
    role.to_s == 'user'
  end

  private

  def set_default_role
    self.role ||= Role.find_by(name: 'user')
  end
end
