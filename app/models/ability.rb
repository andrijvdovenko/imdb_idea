class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, Movie, public: true

    if user.present?
      can [:read], Movie, user_id: user.id
      can [:update], Rating, user_id: user.id

      if user.admin?
        can :manage, :all
      end
    end
  end
end
