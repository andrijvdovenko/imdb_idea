class Category < ApplicationRecord
  has_many :movies

  def to_s
    title
  end
end
