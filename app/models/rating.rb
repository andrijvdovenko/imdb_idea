class Rating < ApplicationRecord
  belongs_to :user
  belongs_to :movie

  validates :rate, numericality: { only_integer: true, greater_than: 0, less_than_or_equal_to: 10 }
  enum rate: %w[0 1 2 3 4 5 6 7 8 9 10]
end
