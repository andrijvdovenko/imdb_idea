$(document).on("change", "#rating", function() {
    $.ajax({
        url: '/movies?movie_id=' + $(this).attr('movie_id') + '&rate=' + $(this).val(),
        type: 'GET',
        format: 'JSON',
        success: function(data) {
            document.open();
            document.write(data);
            document.close();
        }
    });
});
