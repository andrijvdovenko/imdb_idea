Rails.application.routes.draw do
  resources :categories
  devise_for :users
  root to: 'movies#index'
  resources :movies

  # devise_for :users, path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }
end
